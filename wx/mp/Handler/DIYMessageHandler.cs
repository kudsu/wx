﻿using Senparc.NeuChar.Entities;
using Senparc.Weixin.MP.Entities.Request;
using Senparc.Weixin.MP.Entities;
using Senparc.Weixin.MP.MessageContexts;
using Senparc.Weixin.MP.MessageHandlers;

namespace mp.Handler
{
    /// <summary>
    /// 自定义MessageHandler
    /// 把MessageHandler作为基类，重写对应请求的处理方法
    /// </summary>
    public partial class DIYMessageHandler : MessageHandler<DefaultMpMessageContext>
    {
        public DIYMessageHandler(Stream inputStream, PostModel postModel, int maxRecordCount = 0,
            bool onlyAllowEncryptMessage = false, IServiceProvider serviceProvider = null)
            : base(inputStream, postModel, maxRecordCount, onlyAllowEncryptMessage, null, serviceProvider)
        {
        }

        /// <summary>
        /// 所有未处理类型的默认消息
        /// </summary>
        /// <returns></returns>
        public override IResponseMessageBase DefaultResponseMessage(IRequestMessageBase requestMessage)
        {
            //ResponseMessageText也可以是News等其他类型
            var responseMessage = this.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = $"你发送了一条消息，但程序没有指定处理过程";
            return responseMessage;
        }
        public override async Task<IResponseMessageBase> OnTextRequestAsync(RequestMessageText requestMessage)
        {
            //TODO:这里的逻辑可以交给Service处理具体信息，参考OnLocationRequest方法或/Service/LocationSercice.cs
            var responseMessage = CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content =
                string.Format(
                    "您刚才发送了文字信息：{0}\r\n您还可以发送【位置】【图片】【语音】等类型的信息，查看不同格式的回复。\r\nSDK官方地址：http://weixin.senparc.com",
                    requestMessage.Content);
            return responseMessage;
        }

        //public override Task<IResponseMessageBase> OnImageRequestAsync(RequestMessageImage requestMessage)
        //{
        //    //处理图片请求...
        //}

        //public override Task<IResponseMessageBase> OnLocationRequestAsync(RequestMessageLocation requestMessage)
        //{
        //    //处理地理位置请求...
        //}
    }
}
