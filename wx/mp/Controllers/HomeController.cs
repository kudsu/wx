﻿using Microsoft.AspNetCore.Mvc;
using mp.Handler;
using Newtonsoft.Json.Linq;
using Senparc.Weixin.AspNet.MvcExtension;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.Entities.Request;

namespace mp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        public readonly string Token = "weixin";
        //[HttpPost]
        //[ActionName("aa")]
        //public async Task<ActionResult> Index(string signature, string timestamp, string nonce, string echostr)
        //{
        //    if (!CheckSignature.Check(signature, timestamp, nonce, Token))
        //    {
        //        return Content("参数错误！");
        //    }

        //    var ct = new CancellationToken();

        //    var messageHandler = new DIYMessageHandler(Request.InputStream);//接收消息（第一步）
        //    await messageHandler.ExecuteAsync(ct);//执行微信处理过程（关键，第二步）
        //    return new FixWeixinBugWeixinResult(messageHandler);//返回（第三步）
        //}
        /// <summary>
        /// 微信后台验证地址（使用Get），微信后台的“接口配置信息”的Url填写如：http://sdk.weixin.senparc.com/weixin
        /// </summary>
        [HttpGet]
        [ActionName("bb")]
        public ActionResult Get(PostModel postModel, string echostr)
        {
            if (CheckSignature.Check(postModel.Signature, postModel.Timestamp, postModel.Nonce, Token))
            {
                return Content(echostr); //返回随机字符串则表示验证通过
            }
            else
            {
                return Content("failed:" + postModel.Signature + "," + CheckSignature.GetSignature(postModel.Timestamp, postModel.Nonce, Token) + "。" +
                    "如果你在浏览器中看到这句话，说明此地址可以被作为微信公众账号后台的Url，请注意保持Token一致。");
            }
        }
    }
}
